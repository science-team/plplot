Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plplot
Source: https://sf.net/p/plplot/
License: LGPL-2+
Comment: Unless otherwise stated all files are released under the LGPL-2+.
 Any file that has a explicit copyright notice may be distributed
 under the terms of both the LGPL and whatever stated conditions accompany
 the copyright.
Files-Excluded: www doc/doxygen doc/wiki_source

Files: *
Copyright: 1998-2004  Joao Cardoso
 1998-1999, 2001, 2002, 2004  Vince Darley
 1991-2002  Geoffrey Furnish
 1999-2014  Alan W. Irwin
 1998-2005  Rafael Laboissière
 1991-2005  Maurice LeBrun
 2004-2014  Arjen Markus
 2004  Andrew Roach
 2002  David Schleef
 2004-2014  Andrew Ross
 2004, 2005  Thomas J. Duck
 2009 Imperial College, London
 2005 Stephen Leake
 2008 Jerry Bauck
License: LGPL-2+
Comment: The intent behind distributing PLplot under the LGPL is to ensure that it
 continues to evolve in a positive way, while remaining freely
 distributable.  The package is considered a "library" even though there
 are associated programs, such as plrender, pltek, plserver, and pltcl.
 The ties between these programs and the library are so great that I
 consider them as part of the library, so distribution under the terms of
 the LGPL makes sense.  Software developers are allowed and encouraged to
 use PLplot as an integral part of their product, even a commercial
 product.  Under the conditions of the LGPL, however, the PLplot source
 code must remain freely available, including any modifications you make to
 it (if you distribute a program based on the modified library).  Please
 read the full license for more info.

Files: bindings/octave/PLplot/toggle_plplot_use.m
 examples/octave/x0?c.m examples/octave/x1?c.m
 examples/octave/x22c.m examples/octave/p*.m
Copyright: 1998-2003 Joao Cardodo
 2004-2006,2008 Andrew Ross
 2004 Rafael Laboissière
License: GPL-2+
Comment: The octave front end which links to the PLplot library (but not
 vice versa), and which is otherwise independent of PLplot is copyright
 under the GPL license (see COPYING).

Files: utils/plrender.c
 src/plargs.c
Copyright: 1987, 1988 by Digital Equipment Corporation, Maynard, Massachusetts,
 and the Massachusetts Institute of Technology, Cambridge, Massachusett
License: MIT
Comment: The startup code used in argument handling in these files
 is partially derived from "xterm.c" of the X11R5 distribution.

Files: src/mt19937ar.*
Copyright:  1997 - 2002 Makoto Matsumoto and Takuji Nishimura
  2005 Mutsuo Saito
License: BSD-3-clause

Files: cmake/epa_build/ExternalProject.cmake
  cmake/modules/FindLua.cmake
  cmake/modules/ndp_UseQt4.cmake
  cmake/modules/language_support/cmake/CMakeDCompiler.cmake.in
  cmake/modules/language_support/cmake/CMakeD_Copyright.txt
  cmake/modules/language_support/cmake/CMakeDetermineDCompiler.cmake
  cmake/modules/language_support/cmake/CMakeDInformation.cmake
  cmake/modules/language_support/cmake/CMakeTestDCompiler.cmake
  cmake/modules/language_support/cmake/Platform/*
Copyright: 2000-2012 Kitware, Inc.
  2007 Selman Ulug <selman.ulug@gmail.com> and Tim Burrell <tim.burrell@gmail.com>
  2013 Alan W. Irwin
License: BSD-3-clause

Files: include/dirent_msvc.h
Copyright: 2002, 2006-2008 Toni Ronkko
License: Expat

Files: doc/docbook/*
Copyright: 1994  Geoffrey Furnish and Maurice LeBrun
 1999-2013  Alan W. Irwin
 1999, 2000, 2001, 2002, 2003, 2004  Rafael Laboissière
 2003  Joao Cardoso
 2004, 2007  Andrew Ross
 2004  Andrew Roach
 2008-2010  Jerry Bauck
 2008-2009  Hezekiah M. Carty
 2007  Hazen Babcock
 2005  Thomas Duck
License: plplot-doc

Files: data/*.shp
 data/*.shx
Copyright: 2008-2012 Nathaniel Vaughn Kelso and Tom Patterson
License: OS-OpenData
Comment: The shapefiles (.shx and .shp files) supplied with PLplot are
 in the public domain. The data is downloaded from the free resource
 Natural Earth (www.naturalearthdata.com).

Files: lib/nn/* lib/csa/*
Copyright: 2003-2017 Alan W. Irwin,
 2009-2011 Andrew Ross,
 2005-2012 Arjen Markus,
 2005 Geoffrey Furnish,
 2006 Hazen Babcock,
 2010 Hezekiah M. Carty,
 2003 Joao Cardoso,
 2003-2005 Rafael Laboissière,
 2006-2009 Werner Smekal,
 2000-2008 Pavel Sakov and CSIRO
License: BSD-2-Clause

Files: debian/*
Copyright: 2007, 2009, 2010, 2011-2013 Andrew Ross <andrewross@users.sourceforge.net>
           2016 Axel Beckert <abe@debian.org>
           2002 David Schleef <ds@schleef.org>
           2002 LaMont Jones <lamont@debian.org>
           2007 Matej Vela <vela@debian.org>
           2016-2019 Ole Streicher <olebole@debian.org>
           1998-2001, 2003-2008, 2018-2025 Rafael Laboissière <rafael@debian.org>
           2018, 2019 Sébastien Villemot <sebastien@debian.org>
           2007 Steve Langasek <vorlon@debian.org>
           2014 Thibaut Paumard <thibaut@debian.org>
           2024 Matthias Klose <doko@debian.org>
License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1) Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2) Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3) Neither the name of the ORGANIZATION nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-2-Clause
 Redistribution and use of material from the package, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  1. Redistributions of material must retain the above copyright notice, this
     list of conditions and the following disclaimer.
  2. The names of the authors may not be used to endorse or promote products
     derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MIT
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation, and that the names of Digital or MIT not be
 used in advertising or publicity pertaining to distribution of the
 software without specific, written prior permission.
 .
 DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: plplot-doc
 Redistribution and use in source (XML DocBook) and "compiled" forms
 (HTML, PDF, PostScript, DVI, TeXinfo and so forth) with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  1. Redistributions of source code (XML DocBook) must retain the
     above copyright notice, this list of conditions and the following
     disclaimer as the first lines of this file unmodified.
 .
  2. Redistributions in compiled form (transformed to other DTDs,
     converted to HTML, PDF, PostScript, and other formats) must
     reproduce the above copyright notice, this list of conditions and
     the following disclaimer in the documentation and/or other
     materials provided with the distribution.
 .
 Important: THIS DOCUMENTATION IS PROVIDED BY THE PLPLOT PROJECT "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE PLPLOT PROJECT BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS DOCUMENTATION,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: OS-OpenData
 All versions of Natural Earth raster + vector map data found on this
 website are in the public domain. You may use the maps in any manner,
 including modifying the content and design, electronic dissemination,
 and offset printing. The primary authors, Tom Patterson and Nathaniel
 Vaughn Kelso, and all other contributors renounce all financial claim
 to the maps and invites you to use them for personal, educational,
 and commercial purposes.
 .
 No permission is needed to use Natural Earth. Crediting the authors
 is unnecessary.
 .
 However, if you wish to cite the map data, simply use one of the following.
 .
 Short text:
 Made with Natural Earth.
 .
 Long text:
 Made with Natural Earth. Free vector and raster map data @ naturalearthdata.com.
